default["tim-users"]["sudo-group"] = "sysadmin"
default["authorization"]["sudo"]["groups"] = ["sysadmin"]
default["authorization"]["sudo"]["passwordless"] = true
