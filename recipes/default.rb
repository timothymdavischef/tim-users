#
# Cookbook Name:: tim-users
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "sudo"
include_recipe "users"

users_manage node["tim-users"]["sudo-group"] do
    group_id 3001
end

